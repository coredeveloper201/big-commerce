<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class BigCommerceAppController extends Controller
{

    private $store_hash;
    private $headers = [];
    private $url = '';

    public function __construct()
    {
        $this->store_hash = config('services.big_commerce.store_hash');
        $this->headers = [
            'X-Auth-Client' => config('services.big_commerce.client_id'),
            'X-Auth-Token' => config('services.big_commerce.auth_token'),
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ];
        $this->url = 'https://api.bigcommerce.com/stores/';
    }

    public function getProducts()
    {
        $products = Http::withHeaders($this->headers)->get("{$this->url}{$this->store_hash}/v3/catalog/products");
        return response($products);
    }

    public function getProduct($id)
    {
        $product = Http::withHeaders($this->headers)->get("{$this->url}{$this->store_hash}/v3/catalog/products?id={$id}");
        return response($product);
    }

    public function getOrders()
    {
        $orders = Http::withHeaders($this->headers)->get("{$this->url}{$this->store_hash}/v2/orders");
        return response($orders);
    }
}
